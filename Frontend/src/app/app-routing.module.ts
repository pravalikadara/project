// import { NgModule } from '@angular/core';
// import { RouterModule, Routes } from '@angular/router';
// import { LoginComponent } from './login/login.component';
// import { RegisterComponent } from './register/register.component';
// import { HeaderComponent } from './header/header.component';
// import { HomeComponent } from './home/home.component';
// import { AboutComponent } from './about/about.component';
// import { BookingsComponent } from './bookings/bookings.component';
// import { OrdersComponent } from './orders/orders.component';
// import { FrontComponent } from './front/front.component';
// import { Home1Component } from './home1/home1.component';
// import { PaymentComponent } from './payment/payment.component';
// import {ConfimationComponent} from './confimation/confimation.component'
// import { authGuard } from './auth.guard';
// // import { AuthGuard } from '.auth/auth.guard';/



// const routes: Routes = [
//   {path : 'login',component : LoginComponent},
//   {path :'register', component:RegisterComponent},
//   {path :'header', component:HeaderComponent,canActivate: [authGuard] },
//   {path:'home',component:HomeComponent},
//   {path:'about',component:AboutComponent},
//   {path:'booking',component:BookingsComponent},
//   {path:'orders',component:OrdersComponent},
//   {path:'front',component:FrontComponent},
//   { path: 'login', redirectTo: '/header', pathMatch: 'full' },
//   {path:'home1',component:Home1Component},
//   {path:'payment',component:PaymentComponent},
//   {path:'confimation',component:ConfimationComponent},
//   { path: '', redirectTo: '/login', pathMatch: 'full' }
 
 
// ];

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
// export class AppRoutingModule { }
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { BookingsComponent } from './bookings/bookings.component';
import { OrdersComponent } from './orders/orders.component';
import { FrontComponent } from './front/front.component';
import { Home1Component } from './home1/home1.component';

import { AuthGuard } from './auth.guard'; // Assuming you have implemented AuthGuard properly
import { TataComponent } from './tata/tata.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { CarsComponent } from './cars/cars.component';

import { ContactComponent } from './contact/contact.component';
import { AdmincarsComponent } from './admincars/admincars.component';
import { MahindraComponent } from './mahindra/mahindra.component';
import { LogoutComponent } from './logout/logout.component';
import { CustomersComponent } from './customers/customers.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'header', component: HeaderComponent },
  { path: 'home', component: HomeComponent,canActivate:[AuthGuard] },
  { path: 'about', component: AboutComponent },
  { path: 'booking', component: BookingsComponent,canActivate:[AuthGuard] },
  { path: 'orders', component: OrdersComponent },
  { path: 'front', component: FrontComponent,},
  { path: 'login', redirectTo: '/header', pathMatch: 'full' }, // Corrected redirection path
  { path: 'home1', component: Home1Component },
  // { path: 'payment', component: PaymentComponent },
   // Corrected component name
  // { path: '', redirectTo: '/login', pathMatch: 'full' },
  {path:'tata/:id',component:TataComponent},
  {path:'terms',component:TermsComponent},
  {path:'privacy',component:PrivacyComponent},
  {path:'cars',component:CarsComponent},
  {path:'contact',component:ContactComponent},
  {path:'admincars',component:AdmincarsComponent},
  {path:'mahindra',component:MahindraComponent},
  {path:'logout',component:LogoutComponent},
  {path:'customers',component:CustomersComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

