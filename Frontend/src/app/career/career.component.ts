import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-career',
  templateUrl: './career.component.html',
  styleUrl: './career.component.css'
})
export class CareerComponent  implements OnInit{
  images: any[] = [];
  selectedImage: string = '';
  constructor(private empservice: EmpService) { }

  ngOnInit(): void {
    this.empservice.getAllImages().subscribe((data: any) => {
      this.images = data;
    })
  }
}
