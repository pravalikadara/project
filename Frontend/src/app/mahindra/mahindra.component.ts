import { Component } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-mahindra',
  templateUrl: './mahindra.component.html',
  styleUrl: './mahindra.component.css'
})
export class MahindraComponent {
  images: any[] = [];
  selectedImage: string = '';
  constructor(private empservice:EmpService ) { 
    this.images=
    [{imgsrc:'https://imgd-ct.aeplcdn.com/1056x660/n/cw/ec/128413/scorpio-classic-exterior-left-front-three-quarter-2.jpeg?isig=0&q=80'},
    {imgsrc:'https://imgd.aeplcdn.com/664x374/n/cw/ec/131181/bolero-neo-exterior-right-front-three-quarter-2.jpeg?isig=0&q=80'},
    {imgsrc:'https://imgd.aeplcdn.com/664x374/n/cw/ec/128413/scorpio-exterior-right-front-three-quarter-47.jpeg?isig=0&q=80'},
    {imgsrc:'https://imgd-ct.aeplcdn.com/1056x660/n/cw/ec/100615/front-view20.jpeg?isig=0&wm=0&q=80'},
    {imgsrc:'https://imgd-ct.aeplcdn.com/1056x660/n/cw/ec/26918/xuv300-exterior-front-view-2.jpeg?isig=0&q=80'},
    {imgsrc:'https://imgd.aeplcdn.com/370x208/n/cw/ec/40432/scorpio-n-exterior-right-front-three-quarter-75.jpeg?isig=0&q=80'},
    {imgsrc:'https://imgd.aeplcdn.com/370x208/cw/ec/39470/Mahindra-TUV300-Right-Front-Three-Quarter-155763.jpg?wm=0&q=80'}
  ]
  }



}