import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrl: './cars.component.css'
})
export class CarsComponent implements OnInit {
 images: any[] = [];
  selectedImage: string = '';
 
    // Add more images with their associated components here
  
  constructor(private empservice: EmpService ,private router:Router) {
   
   }

  ngOnInit(): void {
    this.empservice.getAllImages().subscribe((data: any) => {
      this.images = data;
      // this.images = data.map((image: any) => ({
      //   imgSrc: image.imgSrc,
      //   component: image.component
      // }));
    })
  
    
  }
 
  navigateToComponentPath(componentPath: string): void {
    this.router.navigate([componentPath]);
  }
}
