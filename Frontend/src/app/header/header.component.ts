import { Component } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})
export class HeaderComponent {
  loginStatus:any;
  userEmail:any;
  adminEmail:any;
  constructor(private service:EmpService){
  }


  ngOnInit(): void {
     this.service.getEmailUser().subscribe((user:any)=>{
      this.userEmail=user.emailId;
    
     })
    this.service.getEmailAdmin().subscribe((admin:any)=>{
         this.adminEmail=admin;
         console.log(this.adminEmail+'the adminEmail')
  
    })
    this.service.getLoginStatus().subscribe((loginStatusData: any) => {
      this.loginStatus = loginStatusData;
    });
  }
isLoggedIn: boolean = false;

logout() {
  this.isLoggedIn = false; 
}

login() {
  this.isLoggedIn = true; 
  }

}
