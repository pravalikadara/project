import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  private ordersSubject = new BehaviorSubject<any[]>([]);
  orders$ = this.ordersSubject.asObservable();

  constructor(private http: HttpClient) { }

  addOrder(order: any): void {
    const orders = this.ordersSubject.getValue();
    orders.push(order);
    this.ordersSubject.next(orders);
    
    // Send the order data to the backend
    this.http.post<any>('http://localhost:8080/registerBooking', order).subscribe(
      response => {
        console.log('Order sent to backend:', response);
        // Optionally, you can perform actions based on the response from the backend
      },
      error => {
        // console.error('Error sending order to backend:', error);
        // Handle error if necessary
      }
    );
  }

  getOrders(): Observable<any[]> {
    return this.orders$;
  }
  deletecustomer(customerid:any){
    return this.http.delete("http://localhost:8085/deleteCustomer/"+customerid)
    }
    deleteOrders(id:any){
      return this.http.delete("http://localhost:8085/deleteOrder/"+id)
    }
}