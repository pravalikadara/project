// import { Component, OnInit } from '@angular/core';

// import { Router } from '@angular/router';
// import { EmpService } from '../emp.service';
// import { OrdersService } from '../orders.service';


// @Component({
//   selector: 'app-admincars',
//   templateUrl: './admincars.component.html',
//   styleUrl: './admincars.component.css'
// })
// export class AdmincarsComponent implements OnInit{
// orders:any;
// constructor(private ordersservice:OrdersService,private empservice:EmpService){}
// ngOnInit(): void {
//   this.empservice.getBookings().subscribe((data: any) => {
//     this.orders = data;
  
//   })

// }
//  deleteOrder(id:any){
//     this.ordersservice.deleteOrders(id).subscribe((data)=>{

//     })
//   }
//   deleteElement(id:any){
//       this.deleteOrder(id)
//       for(var c=0;c<this.orders.length;c++){
//         // console.log('the id of cars ',this.carsdetails[0].carId)
//         if(this.orders[0].id===id){
//                break;
//         }
//        }
    
//        console.log('Delete the index of '+c)
//        this.orders.splice(c,1)
//   }


// }


import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';
import { OrdersService } from '../orders.service';

@Component({
  selector: 'app-admincars',
  templateUrl: './admincars.component.html',
  styleUrls: ['./admincars.component.css'] // Use 'styleUrls' instead of 'styleUrl'
})
export class AdmincarsComponent implements OnInit {
  orders: any;

  constructor(private ordersService: OrdersService, private empService: EmpService) { }

  ngOnInit(): void {
    this.empService.getBookings().subscribe((data: any) => {
      this.orders = data;
      console.log(data)
    });
  }

  deleteOrder(id: any): void {
    this.ordersService.deleteOrders(id).subscribe(() => {
      // Remove the deleted order from the orders array
      this.orders = this.orders.filter((order: any) => order.id !== id);
    });
  }
}
