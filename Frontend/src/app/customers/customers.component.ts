import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrl: './customers.component.css'
})
export class CustomersComponent implements OnInit {
  customers:any;
  constructor( private empservice:EmpService){}
  ngOnInit(): void {
    this.empservice. getAllCustomers().subscribe((data: any) => {
      this.customers = data;
      // this.images = data.map((image: any) => ({
      //   imgSrc: image.imgSrc,
      //   component: image.component
      // }));
    })
 
    }
    deletecustomer(id:any){
      this.empservice.deletecustomer(id).subscribe((data)=>{
  
      })
    }
    deleteElement(id:any){
      this.deletecustomer(id)
      for(var c=0;c<this.customers.length;c++){
        // console.log('the id of cars ',this.carsdetails[0].carId)
        if(this.customers[0].id===id){
               break;
        }
       }
    
       console.log('Delete the index of '+c)
       this.customers.splice(c,1)
  }

}
