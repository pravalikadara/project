import { formatDate } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpService {
  isUserLoggedIn: boolean 
  // isUserLoggedIn:boolean;
loginStatus: Subject<any>;
deleteResult : any;
emailUser:Subject<any>;
emailAdmin:Subject<any>;

  constructor( private http:HttpClient) {
    
    this.isUserLoggedIn=false;
    this.loginStatus = new Subject<any>(); 
    this.emailUser=new Subject<any>();
    this.emailAdmin=new Subject<any>();
   this.isUserLoggedIn =false;
   }
    setUserLoggedIn(){
      this.isUserLoggedIn = true;
    }

    // getUserLoggedStatus(): boolean {
    //   console.log("EmpService: getUserLoggedStatus called, userLoggedIn =", this.isUserLoggedIn);
    //   return this.isUserLoggedIn;
    // }
    getUserLoggedStatus(){
      return this.isUserLoggedIn;
    }
    setUserLogout(){
      return this.isUserLoggedIn=false;
    }
    // setUserEmail(: any): void {
    //   // Implementation goes here
    // }
  
    // setAdminEmail(email: string): void {
    //   // Implementation goes here
    // }
    getEmailUser():any{
      return this.emailUser.asObservable();
   }
   setUserEmail(emailUser:any){
        this.emailUser.next(emailUser);
   }
   getEmailAdmin():any{
    return this.emailAdmin.asObservable();
  }
  setAdminEmail(emailAdmin:any){
      this.emailAdmin.next(emailAdmin);
  }
  
  getLoginStatus(): any {
    return this.loginStatus.asObservable();   
  }
  // constructor(private http: HttpClient) { }

  registerUser(formData: any): Observable<any> {
    return this.http.post("http://localhost:8080/register", formData);
  }

  getAllCountries(): Observable<any> {
    return this.http.get("https://restcountries.com/v3.1/all");
  }
  getAllImages():any{
    return this.http.get("http://localhost:8080/imagesrc")
  }
  Bookings(form:any):any{
    return this.http.post("http://localhost:8080/registerBooking",form)
  }
  getAlltata():any{
    return this.http.get("http://localhost:8080/getallTata",{})
  }
  getAllCustomers():any{
    return this.http.get("http://localhost:8080/getAllCustomers")
  }
  getCustomer(form:any):any{
    return this.http.get("http://localhost:8080/login/"+form.email+"/"+form.password)
  }
  // deleteEmployee(Id: any): any {
  //   return this.http.delete('/deleteEmployee/' + Id)
  // }

  deletecustomer(customerid:any){
    return this.http.delete("http://localhost:8085/deleteCustomer/"+customerid)
    }
getAllCity():any{
  return this.http.get("https://www.google.com/maps/place/India");
}
deleteCar(carId:any):any{
  return this.http.delete("http://localhost:8080/deleteCar/"+carId)
 }
getBookings():any{
  return this.http.get("http://localhost:8080/getAllBookings")
}
}







// isUserLoggedIn:boolean;
// loginStatus: Subject<any>;
// deleteResult : any;
// emailUser:Subject<any>;
// emailAdmin:Subject<any>;
// constructor(private http:HttpClient) { 
//   this.isUserLoggedIn=false;
//   this.loginStatus = new Subject<any>(); 
//   this.emailUser=new Subject<any>();
//   this.emailAdmin=new Subject<any>();
// }
//  getEmailUser():any{
//     return this.emailUser.asObservable();
//  }
//  setUserEmail(emailUser:any){
//       this.emailUser.next(emailUser);
//  }
//  getEmailAdmin():any{
//   return this.emailAdmin.asObservable();
// }
// setAdminEmail(emailAdmin:any){
//     this.emailAdmin.next(emailAdmin);
// }

// getLoginStatus(): any {
//   return this.loginStatus.asObservable();   
// }
