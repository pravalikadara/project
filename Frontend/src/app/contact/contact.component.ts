import { Component } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrl: './contact.component.css'
})
export class ContactComponent {
  submitForm() {
    // Handle form submission logic here
    // For example, send form data to a backend server
  }
}
