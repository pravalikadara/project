import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { BookingsComponent } from './bookings/bookings.component';
import { OrdersComponent } from './orders/orders.component';
import { FrontComponent } from './front/front.component';
// import { RecaptchaCommonModule } from 'ng-recaptcha/lib/recaptcha-common.module';
// import { RecaptchaModule } from 'ng-recaptcha';
import { RecaptchaV3Module } from 'ng-recaptcha';
import { Home1Component } from './home1/home1.component';
import { NgxCaptchaModule } from 'ngx-captcha';
import { AultoComponent } from './aulto/aulto.component';
import { TataComponent } from './tata/tata.component';

import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { CareerComponent } from './career/career.component';
import { CarsComponent } from './cars/cars.component';
import { HundaiComponent } from './hundai/hundai.component';
import { GoogleMapsModule } from '@angular/google-maps';
import { GooglemapComponent } from './googlemap/googlemap.component';
import { ContactComponent } from './contact/contact.component';
import { FooterComponent } from './footer/footer.component';
import { AdminComponent } from './admin/admin.component';
import { MahindraComponent } from './mahindra/mahindra.component';
import { SuzukiComponent } from './suzuki/suzuki.component';
import { LogoutComponent } from './logout/logout.component';
import { CustomersComponent } from './customers/customers.component';
// import { ToastrModule } from 'ngx-toastr';



// import { PaymentComponent } from './payment/payment.component';
// import { ConfimationComponent } from './confimation/confimation.component';
// import { GooglrPayButtonComponent } from './googlr-pay-button/googlr-pay-button.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    HomeComponent,
    AboutComponent,
    BookingsComponent,
    OrdersComponent,
    FrontComponent,
    Home1Component,
    AultoComponent,
    TataComponent,
    TermsComponent,
    PrivacyComponent,
    CareerComponent,
    CarsComponent,
    HundaiComponent,
    GooglemapComponent,
    ContactComponent,
    FooterComponent,
    AdminComponent,
    MahindraComponent,
    SuzukiComponent,
    LogoutComponent,
    CustomersComponent,
   
  
   
    // PaymentComponent,
    // ConfimationComponent,
    // GooglrPayButtonComponent,
   
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    NgxCaptchaModule,
    //  RecaptchaModule//.forRoot()
    RecaptchaV3Module,
    GoogleMapsModule,
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
