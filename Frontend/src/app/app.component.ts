import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  constructor(private http: HttpClient) {}

  submitForm(form: any) {
    // Verify CAPTCHA
    const token = form.value['g-recaptcha-response'];
    if (!token) {
      alert('Please complete the CAPTCHA.');
      return;
    }

    // Send token to your server for verification
    this.http.post<any>('/verify-captcha', { token }).subscribe(response => {
      if (response.success) {
        // CAPTCHA verified, proceed with form submission
        console.log('CAPTCHA verified');
        // Submit form or perform other actions
      } else {
        // CAPTCHA verification failed
        console.error('CAPTCHA verification failed');
      }
    });
  }
//   title = 'angularGooglepay';
//   buttonColor = "black";
//   buttonType = "Buy";
//   isCustomSize = 250;
//   buttonHeight = 50;
//   isTop = window === window.top;

//   paymentRequest = {
//     apiVersion: 2,
//     apiversionMinor: 0,
//     allowedPaymentMethod: [{
//       type: "CARD", // Corrected typo from cARD to CARD
//       parameters: {
//         allowedPaymentMethod: ["PAN_ONLY", "CRYPTOGRAM_3DS"],
//         allowedCardNetworks: ["amex", "visa", "MASTERCARD"]
//       },
//       tokenizationSpecification: {
//         type: "PAYMENT_GATEWAY",
//         parameters: {
//           gateway: "example",
//           gatewayMerchantId: "exampleGatewayMerchantId" // Corrected parameter name
//         }
//       }
//     }],
//     merchantInfo: { // Added missing comma
//       merchantId: "123456789011234567890",
//       merchantName: "demo merchant"
//     },
//     transactionInfo: { // Added missing comma
//       totalPriceStatus: "FINAL",
//       totalPriceLabel: "total",
//       totalPrice: "100.00",
//       currencyCode: "USD",
//       countryCode: "US"
//     }
//   };

//   onLoadPaymentData(event: any) {
//     console.log("load payment data by testcodeiz", event.details);
//   }

}
