import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-tata',
  templateUrl: './tata.component.html',
  styleUrl: './tata.component.css'
})
export class TataComponent  {
  images: any[] = [];
  selectedImage: string = '';
  constructor(private empservice: EmpService) { 
    this.images=
    [{imgsrc:'https://cdni.autocarindia.com/ExtraImages/20220517011850_Nexon%20EV%20Max%20front%20driving%20shot.jpg'},
    {imgsrc:'https://afdcsddealerslist.in/wp-content/uploads/2022/07/Tata-Nexon.png'},
    {imgsrc:'https://tse2.mm.bing.net/th?id=OIP.8B1yS6Yrr2mYnZALrf57ugHaE6&pid=Api&P=0&h=180'},
    {imgsrc:'https://img.indianauto.com/2021/01/07/ZUwJ9d0r/tata-nexon-ev-rear-angle-33e9.jpg'},
    {imgsrc:'https://imgd.aeplcdn.com/0x0/n/cw/ec/41645/nexon-interior-dashboard.jpeg'},
    {imgsrc:'https://www.autobics.com/wp-content/uploads/2017/09/tata-nexon-interior-pr.jpg'},
    {imgsrc:'https://imgd.aeplcdn.com/0X0/n/cw/ec/52163/tata-nexon-front-row-seats57.jpeg?wm=1&q=85'}
  ]
  }

  // ngOnInit(): void {
  //   this.empservice.getAlltata().subscribe((data: any) => {
  //     this.images = data;
  //   })
  // }

}
