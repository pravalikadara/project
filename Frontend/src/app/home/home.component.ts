import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {
  products:any;

  constructor(){
   this.products=[
   {imgs:'https://cdn2.rcstatic.com/images/supplier_logos/greenmotion_logo_lrg.gif'},
   {imgs:'https://cdn2.rcstatic.com/images/supplier_logos/avis_logo_lrg.gif'},
   {imgs:'https://cdn2.rcstatic.com/images/supplier_logos/easirent_logo_lrg.gif'},
   {imgs:'https://cdn2.rcstatic.com/images/supplier_logos/europcar_logo_lrg.gif'},
   {imgs:'https://cdn2.rcstatic.com/images/supplier_logos/sixt_logo_lrg.gif'},
   {imgs:'https://cdn2.rcstatic.com/images/supplier_logos/budget_logo_lrg.gif'},
   {imgs:'https://cdn2.rcstatic.com/images/supplier_logos/dollar_logo_lrg.gif'},
   {imgs:'assets/Images/logo.webp'},
   {imgs:'assets/Images/tata.jpg'},
   {imgs:'assets/Images/tiger.png'},
   {imgs:'assets/Images/ww.jpg'},
   {imgs:'assets/Images/star.png'},
 ]
  
  }
}
