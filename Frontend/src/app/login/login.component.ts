import { Component } from '@angular/core';
import { Router } from '@angular/router';


import { timeout } from 'rxjs';
import { EmpService } from '../emp.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
//   email:any="admin";
// password:any="admin";
email: any = "";
  password: any = "";
customers: any;
customer:any;
constructor(private router :Router,private service : EmpService){
     this.email="admin@gmail.com"
     this.password="password"
}
ngOnInit(): void {
  this.service.getAllCustomers().subscribe((data:any)=>{
 this.customers=data;
 // console.log(data); 
 console.log(this.customers); //

});
}
getAllCustomer(form:any):any{
  this.service.getCustomer(form).subscribe(
    (data:any)=>{
      console.log('getCustomer: ', data);
      this.customer=data;

    console.log('The data',this.customer)}
  )
   return this.customer;
}
loginValidate(form: any) {
  // console.log(form);
  // const email = form.email;
  //   const password = form.password;
 
    this.service.getCustomer(form).subscribe((data: any) => {
      this.customer = data;
      console.log(data); 
      if (this.customer !== null) {
        alert('Login Successfully! '+this.customer.email)
        this.service.setUserLoggedIn();
        this.router.navigate(['booking']);
        this.service.setUserEmail(this.customer.email);
       }
      else if(form.email==this.email&&form.password==this.password){
        alert('welcome to admin page')
        this.router.navigate(['admincars']);
        this.service.setUserLoggedIn();
        this.service.setAdminEmail('admin email after login as admin: '+ form.email);
      }
      else {
        alert('Invalid credentials');
        this.router.navigate(['register'])
      } 
    });
  }
}




