import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
  sitekey: string;
  images: any;

  constructor(private service: EmpService, private router: Router) {
    this.sitekey = '6LeMDJopAAAAAGvWc1nXxlW1TQSbk-evxbQ1jjqF'
  }

  ngOnInit(): void {
    this.service.getAllCountries().subscribe((data: any) => {
      // this.images = data;
    })

  }

  registerValid(form: any): any {
    console.log(form)

    if (!this.isPasswordValid(form.password)) {
      alert('Password must contain at least one lowercase letter, one uppercase letter, and one special character.');
      return;
    }

    this.service.registerUser(form).subscribe(
      (response: any) => {
        alert('User registered successfully!');
        this.router.navigate(['/login'])
      },
      (error: any) => {
        console.error('Error registering user:', error);
      }
    );
  }

  isPasswordValid(password: string): boolean {
    const hasLowercase = /[a-z]/.test(password);
    const hasUppercase = /[A-Z]/.test(password);
    const hasSpecialCharacter = /[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]/.test(password);
    return hasLowercase && hasUppercase && hasSpecialCharacter;
  }

}