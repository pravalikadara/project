import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedInUser: any;

  constructor() { }

  login(username: string, password: string): Observable<boolean> {
    if (username === 'user' && password === 'password') {
      // Assuming successful login
      localStorage.setItem('currentUser', JSON.stringify({ id: 1, name: 'User' }));
      this.loggedInUser = { id: 1, name: 'User' };
      return of(true); // Return an observable of true to indicate successful login
    } else {
      return of(false); // Return an observable of false to indicate failed login
    }
  }

  logout(): void {
    // Clear user information from local storage
    localStorage.removeItem('currentUser');
    this.loggedInUser = null; // Clear the currently logged-in user
  }

  getCurrentUser(): any {
    if (!this.loggedInUser) {
      // If user information is not already loaded, retrieve it from local storage
      const storedUser = localStorage.getItem('currentUser');
      this.loggedInUser = storedUser ? JSON.parse(storedUser) : null;
    }
    return this.loggedInUser;
  }

  isLoggedIn(): boolean {
    return !!this.getCurrentUser(); // Convert to boolean; if user is logged in, returns true, otherwise false
 
  }


}
