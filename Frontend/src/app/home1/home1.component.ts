import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../orders.service';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-home1',
  templateUrl: './home1.component.html',
  styleUrl: './home1.component.css'
})
export class Home1Component  {
  booking: any = {};

  constructor(private ordersService: OrdersService, private router: Router,private authService: AuthService) { }

  submitBookingForm() {
    this.ordersService.addOrder(this.booking);
    this.booking = {};
    this.router.navigate(["/login"]); // Corrected router navigation
    // if (this.authService.isLoggedIn()) {
    //   this.ordersService.addOrder(this.booking);
    //   this.booking = {};
    //   this.router.navigate(["/orders"]);
    // } else {
    //   alert("You must be logged in to book. Please create an account or log in.");
    //   // Optionally, navigate to the login page or display a modal for logging in
    // }
  }
}
