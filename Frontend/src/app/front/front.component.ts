import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-front',
  templateUrl: './front.component.html',
  styleUrl: './front.component.css'
})
export class FrontComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
    // Navigate to the "header" route when the component initializes
    // this.router.navigate(['/header']);
  }
  
}
