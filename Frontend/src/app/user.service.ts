import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiUrl = 'http://localhost:8080/api/users';

  constructor(private http: HttpClient) {}

  registerUser(user: User): Observable<string> {
      return this.http.post<string>(`${this.apiUrl}/register`, user);
  }
  getAllCountires(): any{
    return this.http.get("https://restcountries.com/v3.1/all")
  }
  // getAllImages():any{
  //   return this.http.get("http://localhost:8080/registerImg")
  // }
}
