import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../orders.service';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { EmpService } from '../emp.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css'] // Corrected styleUrl to styleUrls
})
export class BookingsComponent  {
//   // booking: any = {};

//   // constructor(private ordersService: OrdersService, private router: Router) { }

//   // submitBookingForm() {
//   //   // Assuming 'user' object is part of the booking
//   //   this.ordersService.addOrder(this.booking);
//   //   this.booking = {}; // Reset booking object after submission
//   //   this.router.navigate(['/orders']);
//   // }
// // booking: any = {};

// //   constructor(
// //     private ordersService: OrdersService,
// //     private authService: AuthService, // Inject the AuthService
// //     private router: Router
// //   ) { }

// //   submitBookingForm() {
// //     // Check if the user is logged in
// //     if (this.authService.isLoggedIn()) {
// //       // Retrieve the current user's information
// //       const currentUser = this.authService.getCurrentUser();

// //       // Assign the user's information to the booking
// //       this.booking.userId = currentUser.id; // Assuming user object has an 'id' property
// //       this.booking.userName = currentUser.name; // Assuming user object has a 'name' property

// //       // Add the booking
// //       this.ordersService.addOrder(this.booking);

// //       // Clear the booking form
// //       this.booking = {};

// //       // Redirect to the orders component
// //       this.router.navigate(['orders']);
// //     } else {
// //       // User is not logged in, redirect to the login page
// //       // this.router.navigate(['login']);
// //     }
// //   }


// booking: any = {};
// images:any;
// bookingForm: FormGroup;
// cities:any;
// orders:any;

//   constructor(private ordersService: OrdersService, private router: Router,private empservice:EmpService,private formBuilder: FormBuilder) {
//     this.bookingForm = this.formBuilder.group({
//       pickupdate: ['', [Validators.required, this.futureDateValidator(7)]], // Validate for upcoming 7 days
//     });
//   }
//   futureDateValidator(days: number) {
//     return (control: any): { [key: string]: any } | null => {
//       const selectedDate = new Date(control.value);
//       const currentDate = new Date();
//       const futureDate = new Date();
//       futureDate.setDate(currentDate.getDate() + days);

//       if (selectedDate < futureDate) {
//         return null; // Date is within the upcoming days
//       } else {
//         return { 'invalidDate': true }; // Date is not within the upcoming days
//       }
//     };
//   }
   

//   submitBookingForm(book:any) {
//    this.empservice.Bookings(book).subscribe((data:any)=>{

//    })
//     this.router.navigate(["/orders"]); // Corrected router navigation
//   }
// // ngOnInit(): void {
// //   this.empservice.getAllImages().subscribe((data: any) => {
// //     this.images = data;
// //   })
// //   this.empservice.getAllCity().subscribe((data:any)=>{
// //     this.cities=data;
// //   })

booking: any = {};

  constructor(private ordersService: OrdersService, private router: Router) { }

  submitBookingForm() {
    this.ordersService.addOrder(this.booking);
    this.booking = {};
    this.router.navigate(["orders"]); // Corrected router navigation
  }

 }


