// import { Injectable } from "@angular/core";
// import { EmpService } from "./emp.service";
// import {  Router } from "@angular/router";

// @Injectable({   // <-- This is important
//   providedIn: 'root'
// })
// export class AuthGuard{

//   constructor(private service:EmpService,private router:Router) {}

//   canActivate(): boolean {
//     // Check if the user is logged in and return true
// if(this.service.getUserLoggedStatus()){
// return true;
// }else {
//   this.router.navigate(['login'])
// return false;
// }
//     // else navigate to login component and return false

//   }

// }
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { EmpService } from './emp.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private service: EmpService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    console.log("AuthGuard: canActivate called");
    const isLoggedIn = this.service.getUserLoggedStatus();
    console.log("AuthGuard: isLoggedIn =", isLoggedIn);
    if (isLoggedIn) {
      return true;
    } else {
      console.log("AuthGuard: User not logged in, redirecting to login page");
      // Redirect the user to the login page
      return this.router.createUrlTree(['login']);
    }
  }
}
