package Car.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import Car.model.Customers;

@Repository

public interface CustomersRepo extends JpaRepository<Customers,Integer>{
	@Query("select c from Customers c where c.Email =:Email and c.Password = :Password")
	public Customers login(@Param("Email") String Email, @Param("Password") String Password);

}