package Car.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import Car.model.Tata;

@Repository
public interface TataRepo extends JpaRepository<Tata,String> {

}
