package Car.DAO;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Car.model.Bookings;
import Car.model.Customers;

@Service
public class BookingsDAO {
	 @Autowired
	  private BookingsRepo bookingsRepo;
	  
	  public void register(Bookings c1){
			bookingsRepo.save(c1);
		}
	  public List<Bookings> getAllBookings(){
		  return bookingsRepo.findAll();
	  }
	  public List<Bookings>getbookingByName(String username) {
	       
		  return bookingsRepo.findAll();
	    }
	  public void deleteBookings(long id){
		  bookingsRepo.deleteById(id);;
	  }
}
