package Car.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import Car.model.Nexon;




@Repository
public interface NexonRepo extends JpaRepository<Nexon,String>  {

}
