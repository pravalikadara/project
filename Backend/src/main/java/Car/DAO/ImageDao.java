package Car.DAO;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Car.model.Image;

@Service
public class ImageDao {
	@Autowired
	private ImageRepository imagerepository;
	
	public void register(Image image){
		imagerepository.save(image);
	}
	public List<Image> getAllImages(){
		return imagerepository.findAll();
		
	}

	public void delete(long id){
	    imagerepository.deleteById(id);
	}
	
	public void editImage(Image c1){
		imagerepository.save(c1);
	}

}
