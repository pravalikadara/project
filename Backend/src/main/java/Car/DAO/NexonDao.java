package Car.DAO;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import Car.model.Nexon;
import Car.model.Tata;

public class NexonDao {
	@Autowired
	private NexonRepo nexonRepo;
	
	public void register(Nexon image){
		nexonRepo.save(image);
	}
	public List<Nexon> getallNexon(){
		return nexonRepo.findAll();
		
	}
}
