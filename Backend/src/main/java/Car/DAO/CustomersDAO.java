package Car.DAO;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Car.model.Customers;

@Service
public class CustomersDAO {
	 @Autowired
	  private CustomersRepo customerRepo;
	  
	  public void register(Customers customer){
			customerRepo.save(customer);
		}
	  public List<Customers> getAllCustomers(){
		  return customerRepo.findAll();
	  }
	  public Customers getCustomerById(int id) {
	       
	        return customerRepo.findById(id).orElse(null);
	    }
	  public Customers login(String emailId, String password) {
		  Customers cust = customerRepo.login(emailId, password);
		  System.out.println(cust);
		    return cust;
		}
	  public void deleteCustomer(int id){
		  customerRepo.deleteById(id);;
	  }
	 
//	  public void updateImageId(Customers c1){
//		  customerRepo.save(c1);
//	  }

}
