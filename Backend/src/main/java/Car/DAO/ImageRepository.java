package Car.DAO;

import org.springframework.data.jpa.repository.JpaRepository;

import Car.model.Image;

public interface ImageRepository extends JpaRepository<Image, Long> {
}
