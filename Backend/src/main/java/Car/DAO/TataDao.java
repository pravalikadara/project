package Car.DAO;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import Car.model.Tata;

@Service
public class TataDao {
	@Autowired
	private TataRepo tataRepo;
	
	public void register(Tata image){
		tataRepo.save(image);
	}
	public List<Tata> getallTata(){
		return tataRepo.findAll();
		
	}
}
