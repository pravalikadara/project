package Car.DAO;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import Car.model.Bookings;

@Repository
public interface BookingsRepo extends JpaRepository<Bookings, Long> {
	 //List<Bookings> findByUserName(String userName);
}
