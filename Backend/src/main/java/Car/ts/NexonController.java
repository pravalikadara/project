package Car.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import Car.DAO.NexonDao;
import Car.DAO.TataDao;
import Car.model.Nexon;
import Car.model.Tata;

public class NexonController {
	@Autowired
	private NexonDao nexonDao;
	@PostMapping("registernexonimg")
	public void register(@RequestBody Nexon c1){
	     nexonDao.register(c1);
	}
	 @GetMapping("getallnexon")
	   public List <Nexon> showAllNexon(){
		  return nexonDao.getallNexon();
		   
	   }
}
