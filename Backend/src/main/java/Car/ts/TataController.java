package Car.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import Car.DAO.ImageDao;
import Car.DAO.TataDao;
import Car.model.Image;
import Car.model.Tata;
@CrossOrigin("*")
@RestController
public class TataController {
	@Autowired
	private TataDao tataDao;
	@PostMapping("registertataimg")
	public void register(@RequestBody Tata c1){
	     tataDao.register(c1);
	}
	 @GetMapping("getallTata")
	   public List <Tata> showAllTata(){
		  return tataDao.getallTata();
		   
	   }
}
