package Car.ts;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



import Car.DAO.CustomersDAO;
import Car.model.Customers;
@CrossOrigin("*")
@RestController

public class CustomerController {
	@Autowired
	private CustomersDAO customerDao;
	
     
	@PostMapping("register")
	public void register(@RequestBody Customers c1){
	     customerDao.register(c1);
	}
	
	@RequestMapping("getAllCustomers")
	public List <Customers> getAllCustomers(){
		return customerDao.getAllCustomers();
	}
	 @RequestMapping("Customerid/{id}")
	   public Customers Customerid(@PathVariable("id") int id){
		   return customerDao.getCustomerById(id);
	   }

	 @GetMapping("/login/{emailId}/{password}")
		public Customers login(@PathVariable("emailId") String emailId, @PathVariable("password") String password) {
			Customers cust = customerDao.login(emailId, password);
			System.out.println(cust);
		    return cust;
		}
		
		@DeleteMapping("deleteCustomer/{id}")
		public String delete(@PathVariable("id") int id){
			customerDao.deleteCustomer(id);
			return "Customer with id " + id + " is deleted.";
		}
		
//		@PutMapping("updateCarId")
//			public void updateImageId(@RequestBody Customers c1){
//			customerDao.updateImageId(c1);
//		}


	 }

