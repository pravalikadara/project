package Car.ts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RestController;

import Car.DAO.BookingsDAO;
import Car.DAO.CustomersDAO;
import Car.model.Bookings;
import Car.model.Customers;

@CrossOrigin("*")
@RestController
public class BookingController {
	@Autowired
	private BookingsDAO bookingsDao;
	
     
	@PostMapping("registerBooking")
	public void register(@RequestBody Bookings c1){
	     bookingsDao.register(c1);
	}
	
	@RequestMapping("getAllBookings")
	public List <Bookings> getAllBookings(){
		return bookingsDao.getAllBookings();
	}
	 @GetMapping("getbookingByName/{username}")
	    public List<Bookings> getBookingByName(@PathVariable("username") String userName) {
	        return bookingsDao.getbookingByName(userName);
	    }
     @DeleteMapping("deleteOrder/id")
    	 public String delete(@PathVariable("id") int id){
 			bookingsDao.deleteBookings(id);
 			return "Booking with id " + id + " is deleted.";
 		}
     }

