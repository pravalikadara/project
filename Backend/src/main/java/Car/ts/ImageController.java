package Car.ts;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import Car.DAO.ImageDao;
import Car.model.Customers;
import Car.model.Image;
@CrossOrigin(origins="*")
@RestController

public class ImageController {
	@Autowired
	private ImageDao imageDao;
	@PostMapping("registerImg")
	public void register(@RequestBody Image c1){
	     imageDao.register(c1);
	}
	 @GetMapping("imagesrc")
	   public List <Image> showAllImages(){
		  return imageDao.getAllImages();
		   
	   }
	 @DeleteMapping("deleteCar/{id}")
		public String delete(@PathVariable("id") int id){
			imageDao.delete(id);
			return "Car details with id " + id + " is deleted.";
		}
		 @PutMapping("editCar")
		 public void editCar(@RequestBody Image c1){
			 imageDao.editImage(c1);
		 }

}


