package Car.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Image {
	 @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private int id;
	    private String name;
	    private String imgSrc;
	    @JsonIgnore
		@OneToMany(mappedBy="car")
		List<Customers> custlist=new ArrayList<Customers>();
		

	    public Image(){
	    	super();
	    }
		public Image(int id, String name, String imgSrc) {
			super();
			this.id = id;
			this.name = name;
			this.imgSrc = imgSrc;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getimgSrc() {
			return imgSrc;
		}
		public void setimgSrc(String imgSrc) {
			this.imgSrc = imgSrc;
		}
		@Override
		public String toString() {
			return "Image [id=" + id + ", name=" + name + ", imgSrc=" + imgSrc + "]";
		}
	    

}
