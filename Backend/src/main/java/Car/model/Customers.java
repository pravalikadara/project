package Car.model;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;



@Entity
public class Customers {
	@Id
	@GeneratedValue
	
	private int Id;
	private String Name;
	

	private String Gender;
	private String City;
	private int Mobile;

	private String Email;
	private String Password;
    @ManyToOne
    @JoinColumn(name="car")
      Image car;


	public Customers() {
		super();
	}


	public Customers(int id, String name, String gender, String city, int mobile, String email, String password,
			Image car) {
		super();
		Id = id;
		Name = name;
		Gender = gender;
		City = city;
		Mobile = mobile;
		Email = email;
		Password = password;
        car = car;
	}


	public int getId() {
		return Id;
	}


	public void setId(int id) {
		Id = id;
	}


	public String getName() {
		return Name;
	}


	public void setName(String name) {
		Name = name;
	}


	public String getGender() {
		return Gender;
	}


	public void setGender(String gender) {
		Gender = gender;
	}


	public String getCity() {
		return City;
	}


	public void setCity(String city) {
		City = city;
	}


	public int getMobile() {
		return Mobile;
	}


	public void setMobile(int mobile) {
		Mobile = mobile;
	}


	public String getEmail() {
		return Email;
	}


	public void setEmail(String email) {
		Email = email;
	}


	public String getPassword() {
		return Password;
	}


	public void setPassword(String password) {
		Password = password;
	}


	public Image getCar() {
		return car;
	}


	public void setCar(Image car) {
		this.car = car;
	}


	@Override
	public String toString() {
		return "Customers [Id=" + Id + ", Name=" + Name + ", Gender=" + Gender + ", City=" + City + ", Mobile=" + Mobile
				+ ", Email=" + Email + ", Password=" + Password + ", car=" + car + "]";
	}

	

	
		

}
