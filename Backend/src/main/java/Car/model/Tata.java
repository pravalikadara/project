package Car.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Tata {
	 @Id
	    @GeneratedValue
	    private int id;
	    private String imgSrc;
	    private String name;
	    private String cost;
	   private String plateno;
	    public Tata(){
	    	super();
	    }
		public Tata(int id, String imgSrc, String name, String cost, String plateno) {
			super();
			this.id = id;
			this.imgSrc = imgSrc;
			this.name = name;
			this.cost = cost;
			this.plateno = plateno;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getImgSrc() {
			return imgSrc;
		}
		public void setImgSrc(String imgSrc) {
			this.imgSrc = imgSrc;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getCost() {
			return cost;
		}
		public void setCost(String cost) {
			this.cost = cost;
		}
		public String getPlateno() {
			return plateno;
		}
		public void setPlateno(String plateno) {
			this.plateno = plateno;
		}
		@Override
		public String toString() {
			return "Tata [id=" + id + ", imgSrc=" + imgSrc + ", name=" + name + ", cost=" + cost + ", plateno="
					+ plateno + "]";
		}
		
}
