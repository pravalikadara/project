package Car.model;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Bookings {
	
	@Id
	@GeneratedValue
	  private Long id;
	private String username;
	private LocalDate pickupdate;
	private LocalDate returndate;
	private LocalTime pickuptime;
	private LocalTime returntime;
    private String cartype;
    public Bookings(){
    	super();
    }
	public Bookings(Long id, String username, LocalDate pickupdate, LocalDate returndate, LocalTime pickuptime,
			LocalTime returntime, String cartype) {
		super();
		this.id = id;
		this.username = username;
		this.pickupdate = pickupdate;
		this.returndate = returndate;
		this.pickuptime = pickuptime;
		this.returntime = returntime;
		this.cartype = cartype;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public LocalDate getPickupdate() {
		return pickupdate;
	}
	public void setPickupdate(LocalDate pickupdate) {
		this.pickupdate = pickupdate;
	}
	public LocalDate getReturndate() {
		return returndate;
	}
	public void setReturndate(LocalDate returndate) {
		this.returndate = returndate;
	}
	public LocalTime getPickuptime() {
		return pickuptime;
	}
	public void setPickuptime(LocalTime pickuptime) {
		this.pickuptime = pickuptime;
	}
	public LocalTime getReturntime() {
		return returntime;
	}
	public void setReturntime(LocalTime returntime) {
		this.returntime = returntime;
	}
	public String getCartype() {
		return cartype;
	}
	public void setCartype(String cartype) {
		this.cartype = cartype;
	}
	@Override
	public String toString() {
		return "Bookings [id=" + id + ", username=" + username + ", pickupdate=" + pickupdate + ", returndate="
				+ returndate + ", pickuptime=" + pickuptime + ", returntime=" + returntime + ", cartype=" + cartype
				+ "]";
	}
	
	
	
    
}
